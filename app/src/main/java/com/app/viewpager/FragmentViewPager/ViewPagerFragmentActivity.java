package com.app.viewpager.FragmentViewPager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.widget.Toast;

import com.app.viewpager.FragmentViewPager.Fragment.ViewPagerFragment;
import com.app.viewpager.R;
import com.app.viewpager.ViewViewPager.ViewpagerActivity;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerFragmentActivity extends AppCompatActivity {

    private ViewPager vp;
    private MyFragmentViewPagerAdapter myFragmentVPAdapter;
    private List<Fragment> mFragmentList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager_fragment);

        vp = findViewById(R.id.vp);

        initData();     // 初始化数据
        initView();     // 初始化界面

    }

    private void initView() {
        // new一个FragmentAdapter，将保存有Fragment数据的List连接这个构造器
        myFragmentVPAdapter = new MyFragmentViewPagerAdapter(getSupportFragmentManager(), mFragmentList);

        // 将构造好的构造器连接vp
        vp.setAdapter(myFragmentVPAdapter);

        // 设置vp的触发监听
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Toast.makeText(ViewPagerFragmentActivity.this,"当前滑到" + position + "页",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initData() {
        // 初始化数据
        for (int i = 0; i < 3; i++) {
            ViewPagerFragment fragment = ViewPagerFragment.newInstance("传参：这个是界面" + i);
            mFragmentList.add(fragment);
        }
    }
}