package com.app.viewpager.FragmentViewPager;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

/**
 * 项目名称：ViewPager
 * 创始人：Kai_Xuan
 * 创建时间：2022/9/27 14:45
 * 功能：
 */
public class MyFragmentViewPagerAdapter extends FragmentPagerAdapter {

    // 创建一个用于保存Fragment的List
    private List<Fragment> mFragmentList;

    public MyFragmentViewPagerAdapter(@NonNull FragmentManager fm,List<Fragment> FragmentList) {
        super(fm);
        this.mFragmentList = FragmentList;
    }

    // 返回Fragment
    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    // 返回数据大小
    @Override
    public int getCount() {
        return mFragmentList.size();
    }
}
