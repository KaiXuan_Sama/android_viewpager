package com.app.viewpager.ViewViewPager;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

/**
 * 项目名称：ViewPager
 * 创始人：Kai_Xuan
 * 创建时间：2022/9/26 19:25
 * 功能：
 */
public class MyViewViewPagerAdapter extends PagerAdapter {

    private List<ImageView> mImageViews;

    public MyViewViewPagerAdapter(List<ImageView> mImageViews) {
        this.mImageViews = mImageViews;
    }

    // 返回大小
    @Override
    public int getCount() {
        // 以普遍理性而论，在构造函数时传过来的mImageViews的大小不应该为0
        return mImageViews.size();
    }

    // 返回是否相同
    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    // 生成
    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = mImageViews.get(position);
        container.addView(imageView);
        return imageView;
    }

    // 销毁方法
    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        //super.destroyItem(container, position, object);
        container.removeView((View) object);
    }
}
