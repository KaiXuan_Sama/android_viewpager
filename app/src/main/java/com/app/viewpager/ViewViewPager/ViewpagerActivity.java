package com.app.viewpager.ViewViewPager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.viewpager.R;

import java.util.ArrayList;
import java.util.List;

public class ViewpagerActivity extends AppCompatActivity {
/*  配置ViewPager的大致步骤：
*
*       1.创建拥有#指定数据#的构造器 -> 继承PagerAdapter ->
*           重写“返回数量(getCount)”、“返回是否相同(isViewFromObject)”、“生成View(instantiateItem)”、“销毁View(destroyItem)[不使用super]”四种方法
*
*       2.创建和上述#指定数据#相同内容的List -> 提取并存入数据 -> 将赋值完毕的List与上述创建的构造器连接
*
*       3.创建ViewPager布局 -> 通过setAdapter方法将其与连接完毕的构造器相连接
* */

    // 定义ViewPager布局
    private ViewPager mViewPager;

    // 定义用于储存ImageView的List
    private List<ImageView> mImageViews = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);

        mViewPager = findViewById(R.id.vp);

        initData();     // 初始化数据
        initView();     // 初始化布局
    }

    /**
     * 初始化布局方法
     */
    private void initView() {
        // 将刚刚初始化完成的List载入到MyViewPagerImageAdapter适配器中
        MyViewViewPagerAdapter myViewPagerAdapter = new MyViewViewPagerAdapter(mImageViews);

        // 将适配器载入mViewPager组件中（因为MyViewPagerImageAdapter继承自PagerAdapter，所以可以直接使用）
        mViewPager.setAdapter(myViewPagerAdapter);

        // ViewPager的滑动检测（非必要，需要实现相关功能时可以使用）
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Toast.makeText(ViewpagerActivity.this,"当前滑到" + position + "页",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    /**
     * 将drawable中的图片资源数据储存在List中
     */
    private void initData() {
        // 获取图片数据
        int[] ImgIDs = {
                R.drawable.ad_1,
                R.drawable.ad_2,
                R.drawable.ad_3,
        };
        for (int imgID : ImgIDs) {
            // 提取drawable中的图片赋值到imageView对象
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(imgID);
            // 将提取到的图片添加到以ImageView定义的List中去
            mImageViews.add(imageView);
        }

    }

}