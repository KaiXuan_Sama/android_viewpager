package com.app.viewpager;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.app.viewpager.FragmentViewPager.Fragment.ViewPagerFragment;
import com.app.viewpager.FragmentViewPager.ViewPagerFragmentActivity;
import com.app.viewpager.ViewViewPager.ViewpagerActivity;

public class MainActivity extends AppCompatActivity {
    /*  ViewPager的用法
    *       数据 + 适配器 + ViewPager
    *
    *
    * */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void toViewViewpager(View view) {
        Intent intent = new Intent(this, ViewpagerActivity.class);
        startActivity(intent);
    }

    public void toFragmentViewpager(View view) {
        Intent intent = new Intent(this, ViewPagerFragmentActivity.class);
        startActivity(intent);
    }
}